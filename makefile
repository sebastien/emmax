# HAPA makefile

MODE =
INCFLAGS = 
LIBFLAGS = -Wall

ifeq ($(MODE),debug)
    #Debug flags
    COMPFLAGS =  ${INCFLAGS} -g -Wall
    CC = gcc
    CLIBS = /usr/lib/atlas/liblapack.a /usr/lib/atlas/libblas.a /usr/lib/libatlas.a /usr/lib/libg2c.a /usr/lib/libcblas.a /usr/lib/libm.a /usr/lib/libz.a
else
    #Efficient Flags
    COMPFLAGS = ${INCFLAGS} -O2 -Wall
    CC = gcc
    CLIBS = /usr/lib/atlas/liblapack.a /usr/lib/atlas/libblas.a /usr/lib/libatlas.a /usr/lib/libg2c.a /usr/lib/libcblas.a /usr/lib/libm.a /usr/lib/libz.a
endif

all: emmax emmax-kin

.c:
	${CC} ${COMPFLAGS} -o $@ $? ${CLIBS}

clean:
	rm -f *.o emmax
