Source: emmax
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: SteffenSteffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libopenblas-dev | libblas-dev | libblas.so,
               liblapacke-dev,
               zlib1g-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/emmax
Vcs-Git: https://salsa.debian.org/med-team/emmax.git
Homepage: http://genome.sph.umich.edu/wiki/EMMAX
Rules-Requires-Root: no

Package: emmax
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: genetic mapping considering population structure
 EMMAX is a statistical test for large scale human or model organism
 association mapping accounting for the sample structure. In addition
 to the computational efficiency obtained by EMMA algorithm, EMMAX takes
 advantage of the fact that each locus explains only a small fraction of
 complex traits, which allows one to avoid repetitive variance component
 estimation procedure, resulting in a significant amount of increase in
 computational time of association mapping using mixed model.
